<?php

namespace Drupal\key_dropbox\Plugin\KeyType;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\key\Plugin\KeyType\AuthenticationMultivalueKeyType;

/**
 * Defines a key for Dropbox credentials.
 *
 * @KeyType(
 *   id = "dropbox_key",
 *   label = @Translation("Dropbox"),
 *   description = @Translation("Dropbox credentials"),
 *   group = "authentication",
 *   key_value = {
 *     "plugin" = "dropbox"
 *   },
 *   multivalue = {
 *     "enabled" = true,
 *     "fields" = {
 *       "app_key" = {
 *         "label" = @Translation("App Key"),
 *         "required" = true,
 *       },
 *      "app_secret" = {
 *         "label" = @Translation("App Secret"),
 *         "required" = true,
 *       },
 *       "api_token" = {
 *         "label" = @Translation("Access Token"),
 *         "required" = true,
 *       },
 *     }
 *   }
 * )
 */
class DropboxKeyType extends AuthenticationMultivalueKeyType {

  /**
   * {@inheritdoc}
   */
  public static function generateKeyValue(array $configuration) {
    return Json::encode($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function validateKeyValue(array $form, FormStateInterface $form_state, $key_value) {
    if (!is_array($key_value) || empty($key_value)) {
      return;
    }
  }

}
