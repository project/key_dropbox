## Key Dropbox

Key Dropbox is an extension to the Key module.

This module provides a new key type for supporting authentication against the
Dropbox service. This key will capture API token, key and secret.

### Requirements

Requires the Key module.

### Install

Install key_dropbox using a standard method for installing a contributed Drupal
module, either by downloading the package or using composer with

`composer require drupal/key_dropbox`

### Usage

Follow the [Key](https://drupal.org/project/key) documentation for getting and
using keys by adding a `key_select` element to your form in order to select your key.

You can apply a filter to select only dropbox keys:

```php
$form['dropbox_auth'] = [
  '#type' => 'key_select',
  '#title' => $this->t('Dropbox Auth'),
  '#key_filters' => ['type' => 'dropbox_key'],
];
```

### Maintainers

* George Anderson (geoanders) - https://www.drupal.org/u/geoanders
